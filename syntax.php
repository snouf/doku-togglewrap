<?php
/**
 * DokuWiki Plugin togglewrap (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Jonas Fourquier <jonas@mythtv-fr.org>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

class syntax_plugin_togglewrap extends DokuWiki_Syntax_Plugin {

    public function getType() {
        return 'substition';
    }

    public function getPType() {
        return 'normal';
    }

    public function getSort() {
        return 308;
    }

    public function connectTo($mode) {
        $this->Lexer->addSpecialPattern('{{togglewrap>.*?}}',$mode,'plugin_togglewrap');
    }

    public function handle($match, $state, $pos, Doku_Handler &$handler){
        $match = substr($match, 13, -2);
        list ($opts,$label) = explode('|',$match, 2);
        list ($class,$opts) = explode('&', $opts, 2);
        if (!$label) $label = $class;
        $opts =  explode('&',$opts);
        return array($class, $label, $pos, $opts);
    }

    public function render($mode, Doku_Renderer &$renderer, $data) {
        if($mode != 'xhtml') return false;
        list($class, $label, $pos, $opts) = $data;
        $renderer->doc .= '<input id="togglewrap_'.$pos.'" type="checkbox" class="togglewrap" value="'.strtolower($class).'" '.(in_array('checked',$opts) ? 'checked=checked ' : '').' /><label for="togglewrap_'.$pos.'">'.$label.'</label>';
        return true;
    }
}
