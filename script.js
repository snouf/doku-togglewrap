function togglewrap(checkbox, duration) {
    if (checkbox.value.substr(0,4) == 'not_') {
        value = checkbox.value.substr(4);
        reverse = true;
    } else {
        value = checkbox.value;
        reverse = false;
    }
    if ((checkbox.checked && !reverse) || (!checkbox.checked && reverse) ) {
        jQuery('.wrap_'+value).show(duration);
        jQuery('.wrap_not_'+value).hide(duration);
        jQuery('input.togglewrap').each(function() {
            if (this.value == value) {
                jQuery(this).prop('checked',true);
            } else if (this.value == 'not_'+value) {
                jQuery(this).prop('checked',false);
            }
        } );
    } else {
        jQuery('.wrap_'+value).hide(duration);
        jQuery('.wrap_not_'+value).show(duration);
        jQuery('input.togglewrap').each(function() {
            if (this.value == value) {
                jQuery(this).prop('checked',false);
            } else if (this.value == 'not_'+value) {
                jQuery(this).prop('checked',true);
            }
        } );
    }
}

function togglewrapUpdateTOC(duration) {
    var titleHidden = new Array();
    jQuery('input.togglewrap').each(function() {
        if ( (this.value.substr(0,4) == 'not_') && (!this.checked)) {
            value = this.value.substr(4);
            reverse = true;
        } else {
            value = this.value;
            reverse = false;
        }
        $wrap = jQuery('.wrap_'+value);
        $wrap_not = jQuery('.wrap_not_'+value);
        if ((this.checked && !reverse) || (!this.checked && reverse) ) {
            $wrap_not.find("h1, h2, h3, h4, h5, h6").each(function() { titleHidden.push(this.id); } );
        } else {
            $wrap.find("h1, h2, h3, h4, h5, h6").each(function() { titleHidden.push(this.id); } );
        }
    } );
    jQuery("#dw__toc li a").each(function() {
        $this = jQuery(this);
        if (jQuery.inArray($this.attr('href').substr(1), titleHidden) != -1) {
            $this.parent().parent().hide(duration);
        } else {
            $this.parent().parent().show(duration);
        }
    } );
}

jQuery(function() {
    jQuery("input.togglewrap").each(function() { togglewrap (this, 0); } );
    togglewrapUpdateTOC(0);
    jQuery("input.togglewrap").change(function() { togglewrap (this, 400); togglewrapUpdateTOC(400)});

});
